import { useEffect, useState } from "react";
import './App.css';
import WSConnection from './helpers/WebSocketHelper';
import {baseUrl} from './helpers/UrlHelper';
import PlayImage from './assets/images/play.png';
import PlayWhiteImage from './assets/images/play-white.png';
import PauseImage from './assets/images/pause.png';

function App() {
  const [activeMenu, setActiveMenu] = useState({});
  const [kioskData, setKioskData] = useState([]);
  const [kioskSetting, setKioskSetting] = useState({});
  const [screenSaverTimeout, setScreenSaverTimeout] = useState(false);
  const [triggerSensor, setTriggerSensor] = useState(false);
  const params = new URLSearchParams(window.location.search);
  const uniq_id = params.get('uniq_id')
  useEffect(()=> {
    (async function(){
      const modelRequest = await fetch(baseUrl(`/api/v1/bookings/${uniq_id}/model.json`));
      const modelResponse = await(modelRequest.json());
      if(modelResponse?.network?.network_id){
        const {network_id, api_key} = modelResponse?.network;
        const kioskTemplateRequest = await fetch(baseUrl(`/experience_center/api/v1/kiosks/${modelResponse?.kiosk_id}/contents.json?network_id=${network_id}&api_key=${api_key}`));
        const kioskResponse = await (kioskTemplateRequest.json());
        setKioskSetting(kioskResponse.kiosk);
        setKioskData(kioskResponse.data);
        if(kioskResponse?.screensaver){
          setScreenSaverTimeout(kioskResponse?.screensaver?.timeout || false);
        }
        // setActiveMenu(kioskResponse.data.find(x => x.is_default))
      }
      if(modelResponse?.websocket_channel){
        const { websocket_channel:channel, websocket_protocol:protocol, websocket_uri:uri  } = modelResponse;
        WSConnection({uri, protocol, channel,
          callback: (data) => {
            console.log(data);
            if(data?.id === false){
              setActiveMenu({});
              setTriggerSensor(false);
            }
            if(data?.triggerSensor){
              setTriggerSensor(data?.triggerSensor)
            }
          }
        })
      }
    })();
  },[]);
  useEffect(() => {
    if(screenSaverTimeout && Object.keys(activeMenu).length > 0){
      if(!activeMenu.play){
        let screenSaverTimeoutInstance = false;
        screenSaverTimeoutInstance = setTimeout(() => {
          window.chatChannel.send({id: false, play: activeMenu.play});
          setActiveMenu({});
          clearTimeout(screenSaverTimeoutInstance);
        }, (screenSaverTimeout * 60) * 1000)
        return () => {
          clearTimeout(screenSaverTimeoutInstance)
        }
      }
    }
  },[activeMenu])
  const handleActiveMenu = (menu) => {
    console.log(screenSaverTimeout)
    const menu_key = {id: menu.id, play: menu.id === activeMenu?.id ? !activeMenu.play : true};
    setActiveMenu(menu_key);
    window.chatChannel.send(menu_key)
  }
  
  return (
    <div className="grid grid-cols-5">
      {kioskData?.map((menu, index) => {
        return <div key={index} className="h-screen menu" style={{
            backgroundImage: `url(${baseUrl(menu.menu_background_url)})`,
            backgroundSize: 'cover'
          }}
          onClick={e => handleActiveMenu(menu)}
        >
          <div 
            className={`h-full w-full relative background-text-image text-center ${activeMenu?.id === menu.id || triggerSensor === menu.id ? 'active' : ''}`} 
          >
            <div className="w-[202px] absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">{activeMenu?.id === menu.id 
              ? <img src={activeMenu?.play ? PauseImage : PlayImage} className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 w-[calc(158px/2)]" /> 
              : <>{menu.name} <img src={PlayWhiteImage} className="h-[48px] w-[48px] absolute top-1/2 -right-1/2 transform -translate-x-full -translate-y-[70%]" /></>
            }</div>
          </div>
        </div>
      })}
    </div>
  );
}

export default App;
