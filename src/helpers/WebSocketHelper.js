import * as ActionCable from '@rails/actioncable';
import { baseUrl } from './UrlHelper';
const WSConnection = ({uri='/cable', protocol='wss', channel={}, callback=(data) => {}}) => {
   const url = baseUrl(uri);
   window.consumer = ActionCable.createConsumer(`${url.replace(/https|http/g, protocol)}`)
   window.chatChannel = window.consumer.subscriptions.create(channel,{
      received(data){
         callback(data)
      },
      connected(){
         console.log('connected')
      }
   })
}
export default WSConnection;