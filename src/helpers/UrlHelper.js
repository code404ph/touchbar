const baseUrl = (url) => {
   if(process.env.NODE_ENV === "development")
      return `${process.env.REACT_APP_API_URL}${url}`;
   const {protocol, hostname} = window.location;
   console.log(`${protocol}//${hostname}${url}`)
   return `${protocol}//${hostname}${url}`;
}

export {baseUrl};